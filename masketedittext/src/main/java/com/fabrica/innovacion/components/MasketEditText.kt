package com.fabrica.innovacion.components


import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import java.util.*

class MasketEditText : AppCompatEditText, TextWatcher, MoneyEditTextContract.View,
    PhoneEditTextContract.View {

    private lateinit var gsMoneyEditTextPresenter: MoneyEditTextContract.Presenter
    private lateinit var phoneEdiTextPresenter: PhoneEditTextContract.Presenter

    private var textFormat: Int? = 0
    private var isDecimal: Int? = 0
    private var isParenthesis: Boolean? = false

    companion object {
        const val MONEY: Int = 0
        const val PHONE: Int = 1

        const val False = 0
        const val True = 1
        const val Hidden = 2
    }

    constructor(context: Context?) : super(context) {
        setParams()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        val attrs = context.obtainStyledAttributes(attrs, R.styleable.MasketEditText)

        textFormat = attrs.getInt(R.styleable.MasketEditText_editTextFormat, 0)
        isDecimal = attrs.getInt(R.styleable.MasketEditText_isDecimal, 0)
        isParenthesis = attrs.getInt(R.styleable.MasketEditText_parentesis, 0) == 1

        setParams()
    }

    private fun setParams() {
        this.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_CLASS_TEXT
        this.keyListener = DigitsKeyListener.getInstance("0123456789")
        this.addTextChangedListener(this)

        if (textFormat == MONEY) {
            gsMoneyEditTextPresenter =
                MoneyEditTextPresenter(this)
        } else {
            phoneEdiTextPresenter = PhoneEdiTextPresenter(this)
        }
    }

    override fun afterTextChanged(s: Editable?) {
        if (textFormat == MONEY) {
            gsMoneyEditTextPresenter.formatText(s, isDecimal!!)
        } else {
            phoneEdiTextPresenter.formatText(s, isParenthesis!!)
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(
        text: CharSequence?,
        start: Int,
        lengthBefore: Int,
        lengthAfter: Int
    ) {

    }

    override fun cambiaSeleccion(length: Int) {
        setSelection(length)
    }

    override fun cambiaTexto(temp: String) {
        setText(temp)
    }

    override fun cambiarSeleccion(length: Int) {
        setSelection(length)
    }

    override fun cambiarTexto(temp: String) {
        setText(temp)
    }

    override fun obtenerUtlimaSeleccion(): Int {
        return getSelectionEnd()
    }

    //Metodos publicos
    fun setTextFormat(textFormat: Int) {
        this.textFormat = textFormat
        setParams()
    }

    fun setDecimal(isDecimal : Int){
        this.isDecimal = isDecimal
        setParams()
    }

    fun getLongValue(): Long? {
        if (text.toString().isEmpty()){
            return 0
        }
        val arr = Objects.requireNonNull<Editable>(text).toString().split("\\.".toRegex())
            .dropLastWhile { it.isEmpty() }
            .toTypedArray()
        var cleanString = arr[0]
        cleanString = cleanString.replace("[$,]".toRegex(), "")

        return java.lang.Long.parseLong(if (cleanString.isEmpty()) "0" else cleanString)
    }

    fun getDoubleValue(): Double {
        if (text.toString().isEmpty()){
            return 0.0
        }
        var cleanString = Objects.requireNonNull<Editable>(text).toString()
        cleanString = cleanString.replace("[$,]".toRegex(), "")
        return java.lang.Double.parseDouble(if (cleanString.isEmpty()) "0" else cleanString)
    }

    fun getPhoneNumber(): String {
        return if (textFormat == PHONE) phoneEdiTextPresenter.getPhone() else ""
    }

}