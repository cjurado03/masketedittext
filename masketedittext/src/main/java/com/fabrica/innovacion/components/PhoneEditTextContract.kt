package com.fabrica.innovacion.components

import android.text.Editable

interface PhoneEditTextContract {
    interface Presenter {
        fun formatText(s: Editable?, parenthesis: Boolean?)
        fun getPhone(): String

    }

    interface View {
        fun cambiarSeleccion(length: Int)
        fun cambiarTexto(temp: String)

    }
}