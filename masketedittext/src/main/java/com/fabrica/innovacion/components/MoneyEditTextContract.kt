package com.fabrica.innovacion.components

import android.text.Editable

interface MoneyEditTextContract {
    interface Presenter {
        fun formatText(s: Editable?, decimal: Int)
    }

    interface View {
        fun cambiaSeleccion(length: Int)
        fun cambiaTexto(temp: String)
        fun obtenerUtlimaSeleccion(): Int
    }
}