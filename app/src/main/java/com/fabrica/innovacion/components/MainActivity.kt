package com.fabrica.innovacion.components

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnSubmit.setOnClickListener {
            val integer = "${etMoneyEditTextInteger.getLongValue()}"
            val decimal = "${etMoneyEditTextDecimal.getDoubleValue()}"
            val hidden = etMoneyEditTextHidden.text.toString()

            val phone = etPhoneEditText.getPhoneNumber()
            val parentesis = etPhoneEditTextParentesis.text.toString()

            val listResult = ArrayList<String>()
            listResult.add(integer)
            listResult.add(decimal)
            listResult.add(hidden)
            listResult.add(phone)
            listResult.add(parentesis)
            val adapter = ArrayAdapter(this,android.R.layout.simple_list_item_1, listResult)
            lvResult.adapter = adapter
        }
    }
}
